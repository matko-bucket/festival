package com.matkovic.jwd48.festival.web.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;


public class FestivalDTO {

	
	@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;
	
    @NotBlank(message = "obavezno polje.")
	@Length(max = 50)
	private String naziv;

	private String datumPocetka;

    private String datumZavrsetka;
    
	@Positive(message = "mora biti pozitivan broj.")
	private int cena;
    
	private int dostupnihKarata;

    private Long mestoOdrzavanjaId;

    private String mestoOdrzavanjaGrad;


    private String mestoOdrzavanjaDrzava;

	public FestivalDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getDatumPocetka() {
		return datumPocetka;
	}

	public void setDatumPocetka(String datumPocetka) {
		this.datumPocetka = datumPocetka;
	}

	public String getDatumZavrsetka() {
		return datumZavrsetka;
	}

	public void setDatumZavrsetka(String datumZavrsetka) {
		this.datumZavrsetka = datumZavrsetka;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	public int getDostupnihKarata() {
		return dostupnihKarata;
	}

	public void setDostupnihKarata(int dostupnihKarata) {
		this.dostupnihKarata = dostupnihKarata;
	}

	public Long getMestoOdrzavanjaId() {
		return mestoOdrzavanjaId;
	}

	public void setMestoOdrzavanjaId(Long mestoOdrzavanjaId) {
		this.mestoOdrzavanjaId = mestoOdrzavanjaId;
	}

	public String getMestoOdrzavanjaGrad() {
		return mestoOdrzavanjaGrad;
	}

	public void setMestoOdrzavanjaGrad(String mestoOdrzavanjaGrad) {
		this.mestoOdrzavanjaGrad = mestoOdrzavanjaGrad;
	}

	public String getMestoOdrzavanjaDrzava() {
		return mestoOdrzavanjaDrzava;
	}

	public void setMestoOdrzavanjaDrzava(String mestoOdrzavanjaDrzava) {
		this.mestoOdrzavanjaDrzava = mestoOdrzavanjaDrzava;
	}




	


}
