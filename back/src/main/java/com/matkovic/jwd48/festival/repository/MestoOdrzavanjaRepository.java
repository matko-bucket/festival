package com.matkovic.jwd48.festival.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;


@Repository
public interface MestoOdrzavanjaRepository extends JpaRepository<MestoOdrzavanja,Long>{
	
	MestoOdrzavanja findOneById(Long id);
	
}
