package com.matkovic.jwd48.festival.model;

import java.time.LocalDate;
import javax.persistence.*;

@Entity
public class Rezervacija {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	
	@Column
	private int kupljenihKarata;
	
	@Column
	private int ukupnaCena;
	
	@ManyToOne
	private Festival festival;

	public Rezervacija() {
		super();
	}

	public Rezervacija(Long id, int kupljenihKarata, int ukupnaCena, Festival festival) {
		super();
		this.id = id;
		this.kupljenihKarata = kupljenihKarata;
		this.ukupnaCena = ukupnaCena;
		this.festival = festival;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getKupljenihKarata() {
		return kupljenihKarata;
	}

	public void setKupljenihKarata(int kupljenihKarata) {
		this.kupljenihKarata = kupljenihKarata;
	}

	public int getUkupnaCena() {
		return ukupnaCena;
	}

	public void setUkupnaCena(int ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}

	public Festival getFestival() {
		return festival;
	}

	public void setFestival(Festival festival) {
		this.festival = festival;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Rezervacija [id=" + id + ", kupljenihKarata=" + kupljenihKarata + ", ukupnaCena=" + ukupnaCena
				+ ", festival=" + festival + "]";
	}
	


  

	
    
}
