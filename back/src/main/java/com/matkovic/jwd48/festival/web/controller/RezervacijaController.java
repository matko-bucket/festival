package com.matkovic.jwd48.festival.web.controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.support.RezervacijaDtoToRezervacija;
import com.matkovic.jwd48.festival.support.RezervacijaToRezervacijaDTO;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;

import com.matkovic.jwd48.festival.web.dto.FestivalDTO;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.dao.DataIntegrityViolationException;

@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class RezervacijaController {
	
	@Autowired
	RezervacijaService rezervacijaService;
	
	@Autowired
	RezervacijaToRezervacijaDTO toRezervacijaDTO;
	
	@Autowired
	RezervacijaDtoToRezervacija toRezervacija;
	
	
	  @PreAuthorize("hasRole('KORISNIK')")
	  @GetMapping("/{id}")
	  public ResponseEntity<RezervacijaDTO> getOne(@PathVariable Long id){
		  Rezervacija rezervacija = rezervacijaService.findOne(id);

	      if(rezervacija != null) {
	          return new ResponseEntity<>(toRezervacijaDTO.convert(rezervacija), HttpStatus.OK);
	      }else {
	          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	      }
	  }
	
	  @PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	  @GetMapping
	  public ResponseEntity<List<RezervacijaDTO>> getAll(){
		  List<Rezervacija> rezervacije = rezervacijaService.findAll();
	     
	      return new ResponseEntity<>(toRezervacijaDTO.convert(rezervacije), HttpStatus.OK);
	          
	  }
	  

	  
	  @PreAuthorize("hasRole('KORISNIK')")
	  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<RezervacijaDTO> create(@Valid @RequestBody RezervacijaDTO rezervacijaDTO){
		  
		  Rezervacija rezervacija = toRezervacija.convert(rezervacijaDTO);

	      if(rezervacija == null) {
	    	  return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }
	      
	      Rezervacija sacuvano = rezervacijaService.save(rezervacija);
	      return new ResponseEntity<>(toRezervacijaDTO.convert(sacuvano), HttpStatus.CREATED);
	  }
	  
	  @ExceptionHandler(value = DataIntegrityViolationException.class)
	  public ResponseEntity<Void> handle() {
	      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }
	    
}
