package com.matkovic.jwd48.festival.web.controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;
import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.support.MestoOdrzavanjaToMestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.support.RezervacijaToRezervacijaDTO;
import com.matkovic.jwd48.festival.web.dto.MestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;


@RestController
@RequestMapping(value = "/api/mestaOdrzavanja", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class MestoOdrzavanjaController {
	
	@Autowired
	MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Autowired
	MestoOdrzavanjaToMestoOdrzavanjaDTO toDostavljacDTO;
	

    @PreAuthorize("permitAll()")
	@GetMapping
	public ResponseEntity<List<MestoOdrzavanjaDTO>> getAll(){
		   List<MestoOdrzavanja> formati = mestoOdrzavanjaService.findAll();
		      
		   return new ResponseEntity<>(toDostavljacDTO.convert(formati), HttpStatus.OK);
	  }
	  
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
	     return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }
}
