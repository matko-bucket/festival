package com.matkovic.jwd48.festival.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

public class MestoOdrzavanjaDTO {

	@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;
	
    private String grad;

    @NotBlank(message = "obavezno polje.")
	@Length(max = 3)
    private String drzava;

    
    
	public MestoOdrzavanjaDTO() {
		super();
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getGrad() {
		return grad;
	}



	public void setGrad(String grad) {
		this.grad = grad;
	}



	public String getDrzava() {
		return drzava;
	}



	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}




	
    
    

}
