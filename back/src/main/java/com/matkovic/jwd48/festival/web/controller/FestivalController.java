package com.matkovic.jwd48.festival.web.controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;
import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.service.FestivalService;
import com.matkovic.jwd48.festival.support.MestoOdrzavanjaToMestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.support.RezervacijaToRezervacijaDTO;
import com.matkovic.jwd48.festival.support.FestivalDtoToFestival;
import com.matkovic.jwd48.festival.support.FestivalPromenaMestaDtoToFestivalPromenaMesta;
import com.matkovic.jwd48.festival.support.FestivalToFestivalDTO;
import com.matkovic.jwd48.festival.web.dto.MestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;
import com.matkovic.jwd48.festival.web.dto.FestivalDTO;
import com.matkovic.jwd48.festival.web.dto.FestivalPromenaMestaDTO;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;

@RestController
@RequestMapping(value = "/api/festivali", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class FestivalController {

	@Autowired
	FestivalService festivalService;
	
	@Autowired
	RezervacijaService rezervacijaService;
	
	@Autowired
	MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Autowired
	FestivalToFestivalDTO toFestivalDTO;
	
	@Autowired
	FestivalDtoToFestival toFestival;
	
	@Autowired
	FestivalPromenaMestaDtoToFestivalPromenaMesta toFestivalPromenaMesta;
	
    
      @PreAuthorize("permitAll()")
	  @GetMapping
	  public ResponseEntity<List<FestivalDTO>> getAll(
		            @RequestParam(required=false) Long mestoId,
		            @RequestParam(required=false) String nazivFestivala,
		            @RequestParam(value = "pageNo", defaultValue = "0") int pageNo){

	      Page<Festival> page = festivalService.search(mestoId, nazivFestivala, pageNo);
	    		  
	      
	      
	      HttpHeaders headers = new HttpHeaders();
	      headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

	      return new ResponseEntity<>(toFestivalDTO.convert(page.getContent()), headers, HttpStatus.OK);
	  }
	  
	  @PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	  @GetMapping("/{id}")
	  public ResponseEntity<FestivalDTO> getOne(@PathVariable Long id){
		  Festival festival = festivalService.findOne(id);

	      if(festival != null) {
	          return new ResponseEntity<>(toFestivalDTO.convert(festival), HttpStatus.OK);
	      }else {
	          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	      }
	  }
	  
	  @PreAuthorize("hasRole('ADMIN')")
	  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<FestivalDTO> create(@Valid @RequestBody FestivalDTO festivalDTO){
		  
		  Festival festival = toFestival.convert(festivalDTO);

	      if(festival.getNaziv() == null || festival.getNaziv() == "" || festival.getCena() <0) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }

	      Festival sacuvano = festivalService.save(festival);

	      return new ResponseEntity<>(toFestivalDTO.convert(sacuvano), HttpStatus.CREATED);
	  }
	  
	  @PreAuthorize("hasRole('ADMIN')")
	  @DeleteMapping("/{id}")
	  public ResponseEntity<Void> delete(@PathVariable Long id){
		  
		  Festival obrisana = festivalService.delete(id);
	
		    
	      if(obrisana != null) {
	          return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      } else {
	          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	      }
	  }
	  
	  @PreAuthorize("hasRole('ADMIN')")
	  @PutMapping(value= "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<FestivalDTO> update(@PathVariable Long id, @Valid @RequestBody FestivalDTO festivalDTO){

	      if(!id.equals(festivalDTO.getId())) {
	          return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }

	      Festival festival = toFestival.convert(festivalDTO);

	      if(festival.getNaziv() == null || festival.getNaziv().equals("") || festival.getCena() <0) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }

	      Festival sacuvana = festivalService.update(festival);

	      return new ResponseEntity<>(toFestivalDTO.convert(sacuvana), HttpStatus.CREATED);
	  }
	  
	  @PreAuthorize("hasRole('KORISNIK')")
	  @PutMapping(value= "/promenaMesta/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<FestivalDTO> changePlace(@PathVariable Long id, @Valid @RequestBody FestivalPromenaMestaDTO festPromenaMestaDTO){

	      if(!id.equals(festPromenaMestaDTO.getId())) {
	          return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }

	      Festival festival = toFestivalPromenaMesta.convert(festPromenaMestaDTO);

	      if(festival == null || festival.getMestoOdrzavanja() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	      }

	      Festival sacuvana = festivalService.update(festival);

	      return new ResponseEntity<>(toFestivalDTO.convert(sacuvana), HttpStatus.CREATED);
	  }
	  
	  
	    @ExceptionHandler(value = DataIntegrityViolationException.class)
	    public ResponseEntity<Void> handle() {
	        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }
}
