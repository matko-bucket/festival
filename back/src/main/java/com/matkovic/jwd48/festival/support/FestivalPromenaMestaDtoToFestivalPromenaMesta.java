package com.matkovic.jwd48.festival.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.service.FestivalService;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;
import com.matkovic.jwd48.festival.web.dto.FestivalPromenaMestaDTO;

@Component
public class FestivalPromenaMestaDtoToFestivalPromenaMesta implements Converter<FestivalPromenaMestaDTO, Festival>{

	@Autowired
	FestivalService festivalService;
	
	@Autowired
	MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Override
	public Festival convert(FestivalPromenaMestaDTO dto) {
		// TODO Auto-generated method stub
		
		Festival festival = festivalService.findOne(dto.getId());
		 if(festival != null){
			 festival.setMestoOdrzavanja(mestoOdrzavanjaService.findOne(dto.getMestoOdrzavanjaId()));
		 }
		return festival;
	}

}
