package com.matkovic.jwd48.festival.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.repository.RezervacijaRepository;
import com.matkovic.jwd48.festival.repository.FestivalRepository;
import com.matkovic.jwd48.festival.service.FestivalService;

@Service
public class JpaFestivalService implements FestivalService{
	
	@Autowired
	FestivalRepository festivalRepository;

	@Autowired
	RezervacijaRepository rezervacijaRepository;
	
	@Override
	public Festival findOne(Long id) {
		return festivalRepository.findOneById(id);
	}

	@Override
	public List<Festival> findAll() {
		return festivalRepository.findAll();
	}

	@Override
	public Page<Festival> findAll(Pageable pageable) {
		return festivalRepository.findAll(pageable);
	}

	@Override
	public Festival save(Festival festival) {
	
		return festivalRepository.save(festival);
	}

	@Override
	public Festival update(Festival festival) {
		return festivalRepository.save(festival);
	}

	@Override
	public Festival delete(Long id) {
		
		Optional<Festival> festival = festivalRepository.findById(id);
        if(festival.isPresent()){
        	Festival fest = festival.get();
        	fest.getMestoOdrzavanja().getFestivali().remove(fest);
        	
        	festivalRepository.deleteById(id);
        	
            return festival.get();
        }
        return null;
		
	}

	@Override
	public Page<Festival> search(Long mestoId, String nazivFestivala, int pageNo) {
		return festivalRepository.search(mestoId, nazivFestivala, PageRequest.of(pageNo, 4));
	}



}
