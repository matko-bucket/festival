package com.matkovic.jwd48.festival.service;

import java.util.List;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;

public interface MestoOdrzavanjaService {
	
	MestoOdrzavanja findOne(Long id);
	
	List<MestoOdrzavanja> findAll();

}
