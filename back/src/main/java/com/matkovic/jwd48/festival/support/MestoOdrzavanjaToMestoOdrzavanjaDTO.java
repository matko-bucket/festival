package com.matkovic.jwd48.festival.support;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;
import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.web.dto.MestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;


@Component
public class MestoOdrzavanjaToMestoOdrzavanjaDTO implements Converter<MestoOdrzavanja, MestoOdrzavanjaDTO> {

	@Override
	public MestoOdrzavanjaDTO convert(MestoOdrzavanja mestoOdrzavanja) {
		MestoOdrzavanjaDTO dto = new MestoOdrzavanjaDTO();
		
		dto.setDrzava(mestoOdrzavanja.getDrzava());
		dto.setGrad(mestoOdrzavanja.getGrad());
		dto.setId(mestoOdrzavanja.getId());
		
	
		return dto;
	}
	
	public List<MestoOdrzavanjaDTO> convert(List<MestoOdrzavanja> mesta){
        List<MestoOdrzavanjaDTO> DTOs = new ArrayList<>();

        for(MestoOdrzavanja mestoOdrzavanja : mesta) {
        	MestoOdrzavanjaDTO dto = convert(mestoOdrzavanja);
        	DTOs.add(dto);
        }

        return DTOs;
    }

}
