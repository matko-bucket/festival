package com.matkovic.jwd48.festival.enumeration;

public enum TipTakmicenja {

	GRAND_SLAM,
	MASTERS_1000,
	MASTERS_500,
	MASTERS_250
}
