package com.matkovic.jwd48.festival.service;

import java.util.List;

import com.matkovic.jwd48.festival.model.Rezervacija;

public interface RezervacijaService {
	
	Rezervacija findOne(Long id);
	
	List<Rezervacija> findAll();

	Rezervacija save(Rezervacija rezervacija);
	
	
	void delete(Rezervacija rezervacija);
	
	List<Rezervacija> findByFestivalId(Long id);


}
