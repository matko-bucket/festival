package com.matkovic.jwd48.festival.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.matkovic.jwd48.festival.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija,Long>{

	Rezervacija findOneById(Long id);
	
	
	void delete(Rezervacija rezervacija);



	List<Rezervacija> findByFestivalId(Long id);


}
