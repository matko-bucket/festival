package com.matkovic.jwd48.festival.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;
import com.matkovic.jwd48.festival.repository.MestoOdrzavanjaRepository;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;

@Service
public class JpaMestoOdrzavanjaService implements MestoOdrzavanjaService{
	
	@Autowired
	MestoOdrzavanjaRepository mestoOdrzavanjaRepository;

	@Override
	public MestoOdrzavanja findOne(Long id) {
		// TODO Auto-generated method stub
		return mestoOdrzavanjaRepository.findOneById(id);
	}

	@Override
	public List<MestoOdrzavanja> findAll() {
		// TODO Auto-generated method stub
		return mestoOdrzavanjaRepository.findAll();
	}
	

}
