package com.matkovic.jwd48.festival.web.dto;

import javax.validation.constraints.Positive;


public class FestivalPromenaMestaDTO {

	
	@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;
	

    private Long mestoOdrzavanjaId;


	public FestivalPromenaMestaDTO() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getMestoOdrzavanjaId() {
		return mestoOdrzavanjaId;
	}


	public void setMestoOdrzavanjaId(Long mestoOdrzavanjaId) {
		this.mestoOdrzavanjaId = mestoOdrzavanjaId;
	}
    
    

}
