package com.matkovic.jwd48.festival.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.matkovic.jwd48.festival.model.Festival;

@Repository
public interface FestivalRepository extends JpaRepository<Festival,Long>{
	
	Festival findOneById(Long id);

	
	@Query("SELECT f FROM Festival f WHERE " +
	            "(:mestoId IS NULL OR f.mestoOdrzavanja.id = :mestoId) AND " +
	            "(:nazivFestivala IS NULL OR f.naziv LIKE %:nazivFestivala%)")
	Page<Festival> search(@Param("mestoId") Long formatId, @Param("nazivFestivala") String nazivFestivala, Pageable pageable);

	
}
