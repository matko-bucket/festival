package com.matkovic.jwd48.festival.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.service.FestivalService;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;
import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.model.Rezervacija;


import java.util.ArrayList;
import java.util.List;

@Component
public class RezervacijaDtoToRezervacija implements Converter<RezervacijaDTO, Rezervacija>{

	@Autowired
	RezervacijaService rezervacijaService;
	@Autowired
	FestivalService festivalService;
	
	
	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		Rezervacija rezervacija;
		

	        if(dto.getId() == null){
	            rezervacija = new Rezervacija();
	            
	        }else{
	            rezervacija = rezervacijaService.findOne(dto.getId());
	        }

	        if(rezervacija != null){
	        	
	        	Festival festival = festivalService.findOne(dto.getFestivalId());
	        	int cenaPoKarti = festival.getCena();
	        	int dostupneKarteStaro = festival.getDostupnihKarata();
	        	
	        	if (dostupneKarteStaro < dto.getKupljenihKarata()) {
	        		rezervacija = null;
	        	}else {
		        	rezervacija.setFestival(festivalService.findOne(dto.getFestivalId()));
		        	rezervacija.setKupljenihKarata(dto.getKupljenihKarata());
		        	rezervacija.setUkupnaCena(cenaPoKarti*dto.getKupljenihKarata());
		        	
		        	festival.setDostupnihKarata(dostupneKarteStaro - dto.getKupljenihKarata());
		        	
		        	festivalService.update(festival);
	        	} 
	        }
	        return rezervacija;
	}
	
	 private LocalDate getLocalDateTime(String dateTime) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        return LocalDate.parse(dateTime, formatter);
	    }

}
