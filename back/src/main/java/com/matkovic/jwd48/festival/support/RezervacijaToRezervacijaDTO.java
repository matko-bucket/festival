package com.matkovic.jwd48.festival.support;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.model.MestoOdrzavanja;
import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.web.dto.MestoOdrzavanjaDTO;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;



@Component
public class RezervacijaToRezervacijaDTO implements Converter<Rezervacija, RezervacijaDTO>{

	@Override
	public RezervacijaDTO convert(Rezervacija rezervacija) {
		// TODO Auto-generated method stub
		RezervacijaDTO dto = new RezervacijaDTO();
		dto.setId(rezervacija.getId());
		dto.setFestivalId(rezervacija.getFestival().getId());
		dto.setFestivalNaziv(rezervacija.getFestival().getNaziv());
		dto.setKupljenihKarata(rezervacija.getKupljenihKarata());
		dto.setUkupnaCena(rezervacija.getUkupnaCena());
		
		

		
		return dto;
	}
	
	public List<RezervacijaDTO> convert(List<Rezervacija> rezervacije){
        List<RezervacijaDTO> DTOs = new ArrayList<>();

        for(Rezervacija rezervacija : rezervacije) {
        	RezervacijaDTO dto = convert(rezervacija);
        	DTOs.add(dto);
        }

        return DTOs;
    }
	
	
}
