package com.matkovic.jwd48.festival.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.repository.RezervacijaRepository;
import com.matkovic.jwd48.festival.service.RezervacijaService;

@Service
public class JpaRezervacijaService implements RezervacijaService{
	
	@Autowired
	RezervacijaRepository rezervacijaRepository;

	@Override
	public Rezervacija findOne(Long id) {
		// TODO Auto-generated method stub
		return rezervacijaRepository.findOneById(id);
	}

	@Override
	public List<Rezervacija> findAll() {
		// TODO Auto-generated method stub
		return rezervacijaRepository.findAll();
	}

	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		// TODO Auto-generated method stub
		return rezervacijaRepository.save(rezervacija);
	}



	@Override
	public void delete(Rezervacija rezervacija) {
		// TODO Auto-generated method stub
		rezervacijaRepository.delete(rezervacija);;
	}

	@Override
	public List<Rezervacija> findByFestivalId(Long id) {
		// TODO Auto-generated method stub
		return rezervacijaRepository.findByFestivalId(id);
	}




}
