package com.matkovic.jwd48.festival.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.matkovic.jwd48.festival.enumeration.TipTakmicenja;


@Entity
public class MestoOdrzavanja {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    @Column
    private String grad;
    
	@Column(nullable=false)
    private String drzava;

    @OneToMany(mappedBy = "mestoOdrzavanja", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Festival> festivali = new ArrayList<>();

	public MestoOdrzavanja() {
		super();
	}

	public MestoOdrzavanja(Long id, String grad, String drzava, List<Festival> festivali) {
		super();
		this.id = id;
		this.grad = grad;
		this.drzava = drzava;
		this.festivali = festivali;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrad() {
		return grad;
	}

	public void setGrad(String grad) {
		this.grad = grad;
	}

	public String getDrzava() {
		return drzava;
	}

	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}

	public List<Festival> getFestivali() {
		return festivali;
	}

	public void setFestivali(List<Festival> festivali) {
		this.festivali = festivali;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MestoOdrzavanja other = (MestoOdrzavanja) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MestoOdrzavanja [id=" + id + ", grad=" + grad + ", drzava=" + drzava + ", festivali=" + festivali + "]";
	}
    
    
//  @OneToMany(mappedBy = "formatTakmicenja", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
//  private List<Takmicenje> takmicenja = new ArrayList<>();
//  


   


	
}
