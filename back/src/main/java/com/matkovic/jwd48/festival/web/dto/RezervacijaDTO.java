package com.matkovic.jwd48.festival.web.dto;

import javax.validation.constraints.Positive;



public class RezervacijaDTO {

	@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;
    
  
	private int kupljenihKarata;
    
	private int ukupnaCena;

	private Long festivalId;
	
	private String festivalNaziv;

	public RezervacijaDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getKupljenihKarata() {
		return kupljenihKarata;
	}

	public void setKupljenihKarata(int kupljenihKarata) {
		this.kupljenihKarata = kupljenihKarata;
	}

	public int getUkupnaCena() {
		return ukupnaCena;
	}

	public void setUkupnaCena(int ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}

	public Long getFestivalId() {
		return festivalId;
	}

	public void setFestivalId(Long festivalId) {
		this.festivalId = festivalId;
	}

	public String getFestivalNaziv() {
		return festivalNaziv;
	}

	public void setFestivalNaziv(String festivalNaziv) {
		this.festivalNaziv = festivalNaziv;
	}


	
	
    

    

    

	
    

}
