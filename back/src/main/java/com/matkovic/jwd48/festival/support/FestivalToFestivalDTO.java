package com.matkovic.jwd48.festival.support;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.web.dto.FestivalDTO;



@Component
public class FestivalToFestivalDTO implements Converter<Festival, FestivalDTO>{

	@Autowired
	RezervacijaService rezervacijaService;
	
	@Autowired
	MestoOdrzavanjaService mestoOdrzavanjatService;
	
	@Override
	public FestivalDTO convert(Festival festival) {
		FestivalDTO dto = new FestivalDTO();
		
		dto.setCena(festival.getCena());
		dto.setDatumPocetka(festival.getDatumPocetka().toString());
		dto.setDatumZavrsetka(festival.getDatumZavrsetka().toString());
		dto.setDostupnihKarata(festival.getDostupnihKarata());
		dto.setId(festival.getId());
		dto.setMestoOdrzavanjaDrzava(festival.getMestoOdrzavanja().getDrzava());
		dto.setMestoOdrzavanjaGrad(festival.getMestoOdrzavanja().getGrad());
		dto.setMestoOdrzavanjaId(festival.getMestoOdrzavanja().getId());
		dto.setNaziv(festival.getNaziv());
		
		

		
		return dto;	}
	
	public List<FestivalDTO> convert(List<Festival> festivali){
        List<FestivalDTO> DTOs = new ArrayList<>();

        for(Festival festival : festivali) {
        	FestivalDTO dto = convert(festival);
        	DTOs.add(dto);
        }

        return DTOs;
    }

}
