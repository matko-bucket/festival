package com.matkovic.jwd48.festival.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matkovic.jwd48.festival.model.Rezervacija;
import com.matkovic.jwd48.festival.model.Festival;
import com.matkovic.jwd48.festival.service.MestoOdrzavanjaService;
import com.matkovic.jwd48.festival.service.RezervacijaService;
import com.matkovic.jwd48.festival.service.FestivalService;
import com.matkovic.jwd48.festival.web.dto.RezervacijaDTO;
import com.matkovic.jwd48.festival.web.dto.FestivalDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class FestivalDtoToFestival implements Converter<FestivalDTO, Festival>{

	@Autowired
	FestivalService festivalService;
	
	@Autowired
	MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Autowired
	RezervacijaService rezervacijaService;
	
	@Override
	public Festival convert(FestivalDTO dto) {
		Festival festival;
		

        if(dto.getId() == null){
            festival = new Festival();
        }else{
            festival = festivalService.findOne(dto.getId());
        }

        if(festival != null){
        	festival.setCena(dto.getCena());
        	festival.setDatumPocetka(getLocalDateTime(dto.getDatumPocetka()));
        	festival.setDatumZavrsetka(getLocalDateTime(dto.getDatumZavrsetka()));
        	festival.setDostupnihKarata(dto.getDostupnihKarata());
        	festival.setMestoOdrzavanja(mestoOdrzavanjaService.findOne(dto.getMestoOdrzavanjaId()));
        	festival.setNaziv(dto.getNaziv());
        	

        }
        return festival;
	}
	
	 private LocalDate getLocalDateTime(String dateTime) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        return LocalDate.parse(dateTime, formatter);
	    }

}
