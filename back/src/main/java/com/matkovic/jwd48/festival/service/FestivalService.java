package com.matkovic.jwd48.festival.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.matkovic.jwd48.festival.model.Festival;

public interface FestivalService {

	Festival findOne(Long id);
	
	List<Festival> findAll();
	
	 Page<Festival> findAll(Pageable pageable);

	 Festival save(Festival festival);

	 Festival update(Festival festival);

	 Festival delete(Long id);

	    
	 Page<Festival> search(Long mestoId, String nazivFestivala, int pageNo);
	 

}
