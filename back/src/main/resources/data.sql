INSERT INTO adresa (id, ulica, broj) VALUES (1,'Bulevar Cara Lazara', 5);
INSERT INTO adresa (id, ulica, broj) VALUES (2, 'Dalmatinska', 7);

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN',1);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK',2);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK',2);

INSERT INTO mesto_odrzavanja (id, drzava, grad ) VALUES (1, 'SRB', 'Beograd');
INSERT INTO mesto_odrzavanja (id, drzava, grad) VALUES (2, 'MKD', 'Kumanovo');
INSERT INTO mesto_odrzavanja (id, drzava, grad) VALUES (3, 'RUS','Moskva');

INSERT INTO festival (id, cena, datum_pocetka, datum_zavrsetka, dostupnih_karata, naziv, mesto_odrzavanja_id) VALUES (1, 500, '2020-06-21', '2021-07-21', 10, 'naziv1', 1);
INSERT INTO festival (id, cena, datum_pocetka, datum_zavrsetka, dostupnih_karata, naziv, mesto_odrzavanja_id) VALUES (2, 600, '2020-07-21', '2021-08-21', 11, 'naziv2', 1);
INSERT INTO festival (id, cena, datum_pocetka, datum_zavrsetka, dostupnih_karata, naziv, mesto_odrzavanja_id) VALUES (3, 700, '2020-08-21', '2021-09-21', 12, 'naziv3', 2);
INSERT INTO festival (id, cena, datum_pocetka, datum_zavrsetka, dostupnih_karata, naziv, mesto_odrzavanja_id) VALUES (4, 550, '2020-09-21', '2020-10-21', 13, 'naziv4', 2);
INSERT INTO festival (id, cena, datum_pocetka, datum_zavrsetka, dostupnih_karata, naziv, mesto_odrzavanja_id) VALUES (5, 100, '2020-10-21', '2020-11-21', 14, 'naziv5', 3);

INSERT INTO rezervacija (id, kupljenih_karata, ukupna_cena, festival_id) VALUES (1, 1, 500, 1);
INSERT INTO rezervacija (id, kupljenih_karata, ukupna_cena, festival_id) VALUES (2, 1, 500, 1);
INSERT INTO rezervacija (id, kupljenih_karata, ukupna_cena, festival_id) VALUES (3, 2, 1000, 1);
INSERT INTO rezervacija (id, kupljenih_karata, ukupna_cena, festival_id) VALUES (4, 3, 1500, 1);
INSERT INTO rezervacija (id, kupljenih_karata, ukupna_cena, festival_id) VALUES (5, 10, 7000, 3);
