import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, HashRouter as Router, Switch, Redirect } from "react-router-dom";
import { Navbar, Nav, Container, Button } from "react-bootstrap";
import Home from "./components/Home";

import Festivals from './components/Festival/Festivals';
import AddFestival from './components/Festival/AddFestival';
import Login from './components/authorization/Login';
import NotFound from "./components/NotFound";
import {logout} from './services/auth';
import Registration from "./components/registration/Registration";


class App extends React.Component {
  render() {

    const jwt = window.localStorage['jwt'];

    if(jwt){return (
      
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
                Home
            </Navbar.Brand>
            <Nav>
              
              <Nav.Link as={Link} to="/festivals">
              Festivals
              </Nav.Link>


              <Button onClick={()=>logout()}>Logout</Button>
            </Nav>
          </Navbar>
          <Container style={{paddingTop:"25px"}}>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login"  render={()=><Redirect to="/festivals"/>}/>
              <Route exact path="/festivals" component={Festivals} />
              <Route exact path="/festivals/add" component={AddFestival} />

              <Route component={NotFound} />
            </Switch>
          </Container>
        </Router>
      
    );
  }else{
    return( 
      <Router>
          <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                  Home
              </Navbar.Brand>
              <Nav>
                <Nav.Link as={Link} to="/festivals">
                Festivals
                </Nav.Link>
                <Nav.Link as={Link} to="/registration">
                Registration
                </Nav.Link>
                <Nav.Link as={Link} to="/login">
                Login
                </Nav.Link>
              </Nav>
          </Navbar>
          <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login}/>
              <Route exact path="/festivals" component={Festivals} />
              <Route exact path="/registration" component={Registration} />
            </Switch>
          </Container>
      </Router>);
   
  }
      
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
