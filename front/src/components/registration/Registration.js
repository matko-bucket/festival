import React from 'react'
import Axios from "../../apis/Axios";
import {Button, Form, Col, Row} from 'react-bootstrap';

class Registration extends React.Component {

    constructor(props) {
        super(props);
      
        const addresses = []

        const user = {
          korisnickoIme: '',
          eMail: '',
          ime: '',
          prezime: '',
          adresaDTO: -1,
          lozinka: '',
          ponovljenaLozinka: ''
        }

        this.state = { 
            addresses: addresses,
            user: user
        }
      }

    componentDidMount(){
      this.getAdresses()
    }

    async getAdresses(){
      try {
        let result = await Axios.get("/adrese");
        console.log(result)
       
        this.setState({
            addresses: result.data,
          });
      }catch (error) {
        console.log(error);
      }
    }

    valueInputChanged(event){
      const name = event.target.name;
      const value = event.target.value;
      let user = this.state.user;
      if(name === 'adresaId'){
          let address = this.state.addresses.find(address=>address.id==value)
          user['adresaDTO'] = address
      }else{
        user[name] = value
      }
      this.setState(user)
    }

    async create(){
      const userDTO = this.state.user

      console.log(userDTO)
        try {
          let result = await Axios.post("/korisnici", userDTO);
          console.log(result)
          alert('uspesno dodato')
          this.props.history.push("/festivals");
          // window.location.reload()
        }catch (error) {
          console.log(error);
          alert("Couldn't save the task");
        }
    }

    async checkUsername(){
      let username = this.state.user.korisnickoIme
      const config = {
        params: {
          username: username
        }
      }
      try {
        let result = await Axios.get("/korisnici/checkUsername", config);
        console.log(result)
        // if(result.status == 200){
          alert("Username already taken. Please pick another one")
        // }
      }catch (error) {
        console.log(error);
        alert('Username is free')
      }
    }

  render() {
    return(
        <div>

            <h1>User Registration</h1>
            <br/><br/>
        
            <Row className="justify-content-md-left">
              <Col xs="12" md="8" sm="10" >
              <h2>Fill out registration form</h2>
              <Form>
          
                <Form.Group>
                <Form.Label >Username</Form.Label>
                <Form.Control required type="text"  name="korisnickoIme"  onChange={(e)=>this.valueInputChanged(e)} />
                <Button onClick={() => this.checkUsername()}>Check username availability</Button> 

                </Form.Group>

                <Form.Group>
                <Form.Label >Email</Form.Label>
                <Form.Control required  name="eMail" type="email"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Name</Form.Label>
                <Form.Control required  name="ime" type="text"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Last name</Form.Label>
                <Form.Control required  name="prezime" type="text"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Password</Form.Label>
                <Form.Control required name="lozinka" type="password"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Repeat Password</Form.Label>
                <Form.Control required name="ponovljenaLozinka" type="password"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                  <Form.Label>Pick an address</Form.Label>
                    <Form.Control
                      onChange={(event) => this.valueInputChanged(event)}
                      name="adresaId"
                      as="select">
                      <option value={-1}></option>
                      {this.state.addresses.map((address) => {
                      return (
                         <option key={address.id} value={address.id}>
                           {address.ulica}, {address.broj}
                         </option>
                            );
                          })}
                    </Form.Control>
                </Form.Group>



                  <Button 
                  disabled={ this.state.user.adresaDTO === -1 || this.state.user.eMail ==='' || 
                  this.state.user.ime.length < 3 || this.state.user.ime.length > 50 || this.state.user.prezime.length < 3 || this.state.user.prezime.length > 50 || 
                  this.state.user.lozinka != this.state.user.ponovljenaLozinka || this.state.user.lozinka ==='' || this.state.user.korisnickoIme ===''}
                  onClick={(event)=>{this.create(event);}}>Save</Button>
                </Form>
                </Col>
            </Row>
            
      </div>
        
        )
  }
}

export default Registration;