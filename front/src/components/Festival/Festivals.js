import React from "react";
import Axios from "../../apis/Axios";
import {Button, ButtonGroup, Table, Form, Col, Row} from 'react-bootstrap';

class Festivals extends React.Component {
  constructor(props) {
    super(props);

    const search = {
      mestoId: -1,
      nazivFestivala: ""
    }
    let festival = {
      id: -1,
      naziv: '',
      datumPocetka: '',
      datumZavrsetka: '',
      cena: '',
      dostupnihKarata: 0,
      mestoOdrzavanjaDrzava: '',
      mestoOdrzavanjaId: -1,
    }

    const rezervacija = {
      kupljenihKarata: 0,
      dostupnoKarata: 0,
      festivalNaziv: '',
      festivalId: -1
    }


    this.state = { 
      festivali: [],
      svaMestaOdrzavanja: [],
      festival: festival,
      rezervacija: rezervacija,
      pageNo: 0,
      totalPages: 1,
      search: search,
      toggle: false,
      toggleRezervacija: false,
    }
  }

  componentDidMount() {
    this.getFestivali(0);
    this.getMesta()
  }


  async getFestivali(newPageNo) {
   
    const config = {
      params: {
        pageNo: newPageNo
      }
    }

    if(this.state.search.mestoId!=-1){
      config.params['mestoId'] = this.state.search.mestoId
    }
    if(this.state.search.nazivFestivala!= ''){
      config.params['nazivFestivala'] = this.state.search.nazivFestivala
    }

    console.log(config)
    try {
      let result = await Axios.get("/festivali", config);
      console.log(result)
     
      this.setState({
        festivali: result.data,
        pageNo: newPageNo,
        totalPages: result.headers['total-pages'],
        });
    }catch (error) {
      console.log(error);
    }
  }

  async getMesta() {
   
    try {
      let result = await Axios.get("/mestaOdrzavanja");
      console.log(result)
     
      this.setState({
        svaMestaOdrzavanja: result.data,
        });
    }catch (error) {
      console.log(error);
    }
  }

  async promeniMesto(event, id){
    const value = event.target.value;
    const festDTO = this.state.festival
    festDTO['mestoOdrzavanjaId'] = value
    festDTO['id'] = id
    if(festDTO.mestoOdrzavanjaId == -1){
      alert("Morate odabrati mesto odrzavanja festivala!")
      return
    }
    console.log(festDTO)
    try {
      let result = await Axios.put("/festivali/promenaMesta/" + id, festDTO);
      console.log(result)
     
    window.location.reload()
    }catch (error) {
      console.log(error);
    }

  }



  goToAddOnNewPage(){
    this.props.history.push('/festivals/add')
  }

  searchValueInputChange(event){
    const name = event.target.name;
    const value = event.target.value;

    let search = this.state.search
    search[name] = value
    this.setState({search: search})
    this.getFestivali(0)
  }

  deleteFromState(narudzbaId) {
    var orders = this.state.festivali;
    orders.forEach((element, index) => {
        if (element.id === narudzbaId) {
          orders.splice(index, 1);
            this.setState({orders: orders});
        }
    });
  }

  delete(id){
    console.log(id)
     if(window.confirm('Are you sure?')){
        Axios.delete('/festivali/' + id)
        .then(res => {
            // handle success
            console.log(res);
            alert('deleted successfully!');
            // this.deleteFromState(narudzbaId); // ili refresh page-a window.location.reload();
            window.location.reload()
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
        });
     }else
     alert('nije obrisano')
  }

  RezervacijaValueInputChange(event){
    const value = event.target.value;
    console.log(value)
    let rezervacija = this.state.rezervacija
    rezervacija['kupljenihKarata'] = value
    this.setState({rezervacija: rezervacija})
  }

  getCurrentTime() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m <=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

rezervacija(id, dostupnoKarata, naziv){

  if(window.localStorage['jwt']!=null){
    let rezervacija = {
      festivalId: id,
      festivalNaziv: naziv,
      dostupnoKarata: dostupnoKarata,
      kupljenihKarata: 0
    }
    this.setState({toggleRezervacija : true, rezervacija: rezervacija})
  }else{
    this.props.history.push("/login");
  }
}

async posaljiRezervaciju(){
  const rezervacijaDTO = {
    kupljenihKarata: this.state.rezervacija.kupljenihKarata,
    festivalId: this.state.rezervacija.festivalId

  }
console.log(rezervacijaDTO)
  try {
    let result = await Axios.post("/rezervacije", rezervacijaDTO);
    console.log(result)
    alert('uspesno')
    // this.props.history.push("/competitions");
    window.location.reload()
    this.setState({toggleRezervacija: false})
  }catch (error) {
    console.log(error);
    alert("Couldn't save the task");
  }
}

getCurrentTime() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth() + 1;
  var y = date.getFullYear();
  var HH = date.getHours();
  var mm = date.getMinutes();
  console.log(''+(HH <=9 ? '0' + HH : HH)+':'+(mm <=9 ? '0' + mm : mm))
  return '' + y + '-' + (m <=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

canCreateTask(datumZavrsetka){
  var trenutniDatum = this.getCurrentTime();
  const task = this.state.task
  return trenutniDatum < datumZavrsetka
}

  render() {
    const jwt = window.localStorage['jwt'];

    return (
      <div>
        <br/><br/>
        <Row>
          <Col>

            <Button variant="info" onClick={()=>{this.setState({toggle:!this.state.toggle})}}>Search</Button>&nbsp;

            {
              this.state.toggle ? 
              <div>

                <Form style={{marginTop:35}}>

                  <Row>
                    <Col md={6}>
                      <Form.Group>
                        <Form.Label>Mesto odrzavanja</Form.Label>
                        <Form.Control
                          onChange={(event) => this.searchValueInputChange(event)}
                          name="mestoId"
                          value={this.state.search.mestoId}
                          as="select">
                          <option value={-1}></option>
                          {this.state.svaMestaOdrzavanja.map((mesto) => {
                            return (
                              <option key={mesto.id} value={mesto.id}>
                                {mesto.grad}
                              </option>
                            );
                          })}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row>
                    <Col md={6}>
                      <Form.Group>
                      <Form.Label>Naziv festivala</Form.Label>
                      <Form.Control
                        value={this.state.search.nazivFestivala}
                        name="nazivFestivala"
                        as="input"
                        type="text"
                        onChange={(e) => this.searchValueInputChange(e)}
                      ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>

                  {/* { <Button onClick={() => this.getFestivali(0)}>Search</Button> } */}
                </Form>
              </div> : null
            }
          </Col>
        </Row>
        <br/>


          <h1>Festivali</h1>

          {window.localStorage['role']=="ROLE_ADMIN"?
                        
            <Button className="float-right" variant="primary" onClick={() => this.goToAddOnNewPage()}>Add new Festival</Button>
                        
                        :null }

          <br/><br/>

          <Table striped hover>
           
              <thead className="table-dark" >
                <tr >
                  <th>Naziv festivala</th>
                  <th>Mesto odrzavanja</th>
                  <th>Datum pocetka</th>
                  <th>Datum zavrsetka</th>
                  <th>Cena karte</th>
                  <th>Broj preostalih karata</th>
                  <th>Akcije</th>

                </tr>
              </thead>
           
            <tbody >
              {this.state.festivali.map((festival) => {
                return (
                  
                  <tr key={festival.id} >
                    <td>{festival.naziv}</td>

                    <Row>
                    <Col xs="auto">
                      <Form.Group>
                        <Form.Label>Mesto odrzavanja</Form.Label>
                        <Form.Control disabled={window.localStorage['jwt']==null}
                          onChange={(event) => this.promeniMesto(event, festival.id)}
                          name="mestoOdrzavanjaId"
                          value={festival.mestoOdrzavanjaId}
                          as="select">

                          <option value={-1}></option>
                          {this.state.svaMestaOdrzavanja.map((mesto) => {
                            return (
                              <option key={mesto.id} value={mesto.id}>
                                {mesto.drzava}, {mesto.grad}
                              </option>
                            );
                          })}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    </Row>                    
                  
                    <td>{festival.datumPocetka}</td>
                    <td>{festival.datumZavrsetka}</td>
                    <td>{festival.cena}</td>
                    <td>{festival.dostupnihKarata}</td>

                      {window.localStorage['role']=="ROLE_KORISNIK"?
                      [<td>
                          {festival.dostupnihKarata >0 && this.canCreateTask(festival.datumZavrsetka)?
                            <Button  variant="success" onClick={() => this.rezervacija(festival.id, festival.dostupnihKarata, festival.naziv)}>Rezervisi karte</Button>
                            :null
                          }
                        </td>]
                      :null}

                      {window.localStorage['role']=="ROLE_ADMIN"?
                        [<td>
                          <Button variant="danger" onClick={() => this.delete(festival.id)}>Delete</Button>
                        </td>] 
                        :null }
                  
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <ButtonGroup>
            <Button disabled={this.state.pageNo===0} onClick={()=> this.getFestivali(this.state.pageNo-1)}>Prev</Button>
            <Button disabled={this.state.pageNo===this.state.totalPages-1 || this.state.totalPages ==0} onClick={()=> this.getFestivali(this.state.pageNo+1)}>Next</Button>
          </ButtonGroup>


        {
              this.state.toggleRezervacija ? 
              <div>

                <Form style={{marginTop:35}} >

                  <Row>
                    <Col md={6}>
                      <Form.Group>
                      <Form.Label>Broj karata za festival: {this.state.rezervacija.festivalNaziv} (max {this.state.rezervacija.dostupnoKarata})</Form.Label>
                      <Form.Control
                        autoFocus={true}
                        name="kupljenihKarata"
                        as="input"
                        type="number"
                        onChange={(e) => this.RezervacijaValueInputChange(e)}
                      ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Button disabled={this.state.rezervacija.kupljenihKarata < 1 || this.state.rezervacija.dostupnoKarata < this.state.rezervacija.kupljenihKarata} onClick={() => this.posaljiRezervaciju()}>Rezervisi</Button> 
                </Form>
              </div> : null
            }
      </div>
    );
  }
}

export default Festivals;