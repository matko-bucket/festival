import React from 'react';
import { Form, Row, Col, Button} from 'react-bootstrap';
import Axios from '../../apis/Axios';

class AddFestival extends React.Component {

    constructor(props){
        super(props);

    
     
    let festival = {
      naziv: '',
      datumPocetka: '',
      datumZavrsetka: '',
      cena: '',
      dostupnihKarata: 0,
      mestoOdrzavanjaDrzava: '',
      mestoOdrzavanjaId: -1,
    }

        this.state = { festival: festival, svaMestaOdrzavanja: [] };
    }

    componentDidMount(){
        this.getMestaOdrzavanja();
    }

    async getMestaOdrzavanja() {
   
      try {
        let result = await Axios.get("/mestaOdrzavanja");
        console.log(result)
       
        this.setState({
          svaMestaOdrzavanja: result.data,
          });
      }catch (error) {
        console.log(error);
      }
    }

    
      async create(){
        const festivalDTO = this.state.festival
        if (this.state.festival.datumPocetka > this.state.festival.datumZavrsetka){
          alert("Datum zavrsetka festivala ne moze biti pre datuma pocetka festivala!")
          return
        }
        console.log(festivalDTO)
          try {
            let result = await Axios.post("/festivali/", festivalDTO);
            console.log(result)
            alert('uspesno dodato')
            this.props.history.push("/festivals");
            // window.location.reload()
          }catch (error) {
            console.log(error);
            alert("Couldn't save the task");
          }
      }

      valueInputChanged(event){
        const name = event.target.name;
        const value = event.target.value;
        
        let festival = this.state.festival;
        festival[name] = value
        this.setState(festival)
      }


    render(){
        return (
            <>
            <Row className="justify-content-md-left">
              <Col xs="12" md="8" sm="10" >
              <h2>Festival</h2>
              <Form>
          
                <Form.Group>
                <Form.Label >Naziv festivala</Form.Label>
                <Form.Control required type="text"  name="naziv"  onChange={(e)=>this.valueInputChanged(e)} />
                </Form.Group>

                <Form.Group>
                <Form.Label >Datum pocetka festivala</Form.Label>
                <Form.Control required  name="datumPocetka" type="date"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Datum zavrsetka festivala</Form.Label>
                <Form.Control required  name="datumZavrsetka" type="date"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Cena karte</Form.Label>
                <Form.Control required  name="cena" type="number"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>

                <Form.Group>
                <Form.Label >Ukupan broj dostupnih karata</Form.Label>
                <Form.Control required name="dostupnihKarata" type="number"  onChange={(e)=>this.valueInputChanged(e)} /> 
                </Form.Group>


                <Form.Group>
                <Form.Label  >Mesto odrzavanja </Form.Label>
                <Form.Control as="select"  name="mestoOdrzavanjaId"  onChange={event => this.valueInputChanged(event)}>
                    <option value={-1}></option>
                        {
                            this.state.svaMestaOdrzavanja.map((mesto) => {
                                return (
                                    <option key={mesto.id} value={mesto.id}>{mesto.grad}</option>
                                )
                            })
                        }
                </Form.Control>
                </Form.Group>

                  <Button 
                  disabled={ this.state.festival.naziv ==='' || this.state.festival.datumPocetka ==='' || 
                  this.state.festival.dostupnihKarata < 0 || this.state.festival.datumZavrsetka ==='' || this.state.festival.mestoOdrzavanjaId <=-1 }
                  onClick={(event)=>{this.create(event);}}>Save</Button>
                </Form>
                </Col>
            </Row>
            </>
        )
    }
}

export default AddFestival;